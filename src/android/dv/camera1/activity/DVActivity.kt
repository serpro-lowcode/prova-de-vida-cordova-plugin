package br.gov.serpro.dv.camera1.activity

//import kotlinx.android.synthetic.main.toolbar.*
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import br.gov.serpro.ProvaDeVidaPlugin.DV_DATA
import br.gov.serpro.ProvaDeVidaPlugin.IS_ALIVE
import br.gov.serpro.R
import br.gov.serpro.dv.camera1.fragment.DVFragment
import java.io.ByteArrayOutputStream

/**
 * TODO Detecção de Vida
 * Activity que demonstra o uso do [DVFragment], responsável pelo processo de detecção de vida.
 *
 * TODO Melhorar processo de Pedido de Permissão à Acessar a Camera (use PermissionUtils)
 */
class DVActivity :  AppCompatActivity(), DVFragment.Delegate {

    companion object {
        private const val QUALIDADE_COMPRESSAO_PADRAO = 25

        const val INTENT_PARAM_QUALIDADE_COMPRESSAO = "INTENT_PARAM_QUALIDADE_COMPRESSAO"

        @JvmStatic
        fun newIntent(context: Context, qualidadeCompressao: Int = QUALIDADE_COMPRESSAO_PADRAO): Intent =
                Intent(context, DVActivity::class.java).apply {
                    putExtra(INTENT_PARAM_QUALIDADE_COMPRESSAO, qualidadeCompressao)
                }
    }

    /**
     * Qualidade da compressão usada na imagem resultante da prova de vida.
     */
    private var qualidadeCompressao = 25

    /**
     * [AppCompatActivity.onCreate]
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dv_activity)
//        setContentView(resources.getIdentifier("dv_activity", "layout", packageName))

        this.qualidadeCompressao = intent?.getIntExtra(INTENT_PARAM_QUALIDADE_COMPRESSAO, QUALIDADE_COMPRESSAO_PADRAO)
                ?: QUALIDADE_COMPRESSAO_PADRAO

        setupToolbar()
    }

    private fun setupToolbar() {
        //setSupportActionBar(toolbar)
        supportActionBar?.title = "Biometria"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * [DVFragment.Delegate]
     */
    override fun onSucessoDeteccaoDeVida(face: Bitmap) {
        //logarInfo("Detecção de vida concluída com sucesso!")

        val stream = ByteArrayOutputStream()
        face.compress(Bitmap.CompressFormat.JPEG, this.qualidadeCompressao, stream)
        val byteArray = stream.toByteArray()

        val returnIntent = Intent()
        returnIntent.putExtra(DV_DATA, byteArray)
        returnIntent.putExtra(IS_ALIVE, true)

        setResult(RESULT_OK, returnIntent)
        finish()

    }

    /**
     * [DVFragment.Delegate]
     */
    override fun onErroDuranteDeteccao(e: Exception) {
        //logarErro(e, "Erro durante detecção de vida")
        val returnIntent = Intent()
        returnIntent.putExtra(IS_ALIVE, false)
        setResult(RESULT_CANCELED, returnIntent)
        finish()
    }
}
