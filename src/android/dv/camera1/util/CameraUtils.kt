package br.gov.serpro.dv.camera1.util

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.*
import android.hardware.Camera
import android.view.Surface
import android.view.WindowManager
import br.gov.serpro.dv.util.logarErro
import br.gov.serpro.dv.util.logarInfo
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import java.io.ByteArrayOutputStream


object CameraUtils {
    private const val TAG = "CameraUtils"

    /**
     * Check if this device has a camera.
     *
     * @see https://developer.android.com/guide/topics/media/camera
     */
    fun checkCameraHardware(context: Context): Boolean =
            context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)

    /**
     * Tenta obter o Id da primeira camera frontal.
     *
     * @return null Caso nenhuma câmera frontal seja encontrada
     */
    fun selecionarPrimeiraCameraFrontal(): Int? {
        val camCount = Camera.getNumberOfCameras()

        val camInfo = Camera.CameraInfo()
        (0..camCount).forEach { i ->
            Camera.getCameraInfo(i, camInfo)
            if (camInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                return i
            }
        }

        return null
    }

    /**
     * TODO Analisar se este método é melhor estar na [DVActivity] ou aqui mesmo...
     *
     * @see https://developer.android.com/reference/android/hardware/Camera#setDisplayOrientation(int)
     */
    fun calcularCameraDisplayOrientation(context: Context, cameraId: Int, camera: Camera): Int {

        val info = Camera.CameraInfo()
        Camera.getCameraInfo(cameraId, info)

        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val rotation = windowManager.defaultDisplay.rotation

        val degrees = when (rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> 0
        }

        logarInfo("Orientação do Dispositivo: $degrees")

        val result: Int = if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            val tmp = (info.orientation + degrees) % 360
            (360 - tmp) % 360  // compensate the mirror
        } else {  // back-facing
            (info.orientation - degrees + 360) % 360
        }

        return result
    }

    /**
     * Retorna a rotação de compensação para o preview da câmera
     */
    fun getCameraPreviewRotationCompensationFromCameraId(cameraId: Int): Int {
        // Define a rotação de ajuste necessária para a imagem de acordo com a orientação
        // da camera
        val info = Camera.CameraInfo()
        Camera.getCameraInfo(cameraId, info)

        return if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            270
        } else {
            90
        }
    }

    /**
     * Retorna a rotação de compensação do Firebase para para conversão
     */
    fun getFirebaseRotationCompensationFromCameraId(cameraId: Int): Int =
            when (getCameraPreviewRotationCompensationFromCameraId(cameraId)) {
                0 -> FirebaseVisionImageMetadata.ROTATION_0
                90 -> FirebaseVisionImageMetadata.ROTATION_90
                180 -> FirebaseVisionImageMetadata.ROTATION_180
                270 -> FirebaseVisionImageMetadata.ROTATION_270
                else -> FirebaseVisionImageMetadata.ROTATION_0
            }

    fun getSupportedPreviewFormatLabel(previewFormatCode: Int): String =
            when (previewFormatCode) {
                ImageFormat.NV16 -> "NV16"
                ImageFormat.NV21 -> "NV21"
                ImageFormat.JPEG -> "JPEG"
                ImageFormat.RGB_565 -> "RGB_565"
                ImageFormat.YUY2 -> "YUY2"
                ImageFormat.YV12 -> "YV12"
                ImageFormat.UNKNOWN -> "UNKNOWN"
                else -> "Formato não identificado (novo ou desconhecido)"
            }

    /**
     * Obtém o melhor preview size para ser usado pela câmera, com base na lista de preview sizes
     * suportados pelo device.
     *
     * Borrowed from https://stackoverflow.com/questions/17804309/android-camera-preview-wrong-aspect-ratio
     */
    fun getOptimalPreviewSize(sizes: List<Camera.Size>?, w: Int, h: Int): Camera.Size? {
        val aspectTolerance = 0.05
        val targetRatio = w.toDouble() / h

        if (sizes == null) return null

        var optimalSize: Camera.Size? = null

        var minDiff = java.lang.Double.MAX_VALUE

        // Find size
        for (size in sizes) {
            val ratio = size.width.toDouble() / size.height
            if (Math.abs(ratio - targetRatio) > aspectTolerance) continue
            if (Math.abs(size.height - h) < minDiff) {
                optimalSize = size
                minDiff = Math.abs(size.height - h).toDouble()
            }
        }

        if (optimalSize == null) {
            minDiff = java.lang.Double.MAX_VALUE
            for (size in sizes) {
                if (Math.abs(size.height - h) < minDiff) {
                    optimalSize = size
                    minDiff = Math.abs(size.height - h).toDouble()
                }
            }
        }
        return optimalSize
    }

    fun getSlowestPreviewFpsRange(supportedPreviewFpsRange: List<IntArray>?): IntArray? {
        if (supportedPreviewFpsRange == null || supportedPreviewFpsRange.isEmpty()) {
            return null
        }

        var selectedRange = supportedPreviewFpsRange[0]

        supportedPreviewFpsRange.forEach { range ->
            val maxFps = range[1]
            val maxSelectedFps = selectedRange[1]

            if (maxFps < maxSelectedFps) {
                selectedRange = range
            }
        }

        return selectedRange
    }

    /**
     * Coverte o ByteArray obtido do Preview da Camera para um Bitmap utilizando como compressão
     * o formato JPEG/JPG.
     *
     * Baseado nos links:
     * https://www.programcreek.com/java-api-examples/?class=android.graphics.ImageFormat&method=NV21
     * https://forums.xamarin.com/discussion/40991/onpreviewframe-issue-converting-preview-byte-to-android-graphics-bitmap
     * https://stackoverflow.com/questions/7620401/how-to-convert-byte-array-to-bitmap
     * https://stackoverflow.com/questions/3528735/failed-binder-transaction-when-putting-an-bitmap-dynamically-in-a-widget
     * https://stackoverflow.com/questions/14538871/the-buffer-size-for-callbackbuffer-of-android-camera-need-to-be-8-time-as-big-as/14541801
     */
    fun bitmapJpegFromCameraPreviewFrame(camera: Camera,
                                         cameraId: Int,
                                         frame: ByteArray,
                                         quality: Int = 100): Bitmap? {
        try {
            val previewFormat = camera.parameters.previewFormat
            val previewSize = camera.parameters.previewSize
            val width = previewSize.width
            val height = previewSize.height

            val yuvImage = YuvImage(frame, previewFormat, width, height, null)

            val outputStream = ByteArrayOutputStream(frame.size)

            // "try-with-resources" do kotlin utiliza o método "use"
            // https://www.baeldung.com/kotlin-try-with-resources
            var bitmap: Bitmap? = null
            outputStream.use { stream ->
                yuvImage.compressToJpeg(Rect(0, 0, width, height), quality, stream)


                bitmap = BitmapFactory.decodeByteArray(stream.toByteArray(), 0, stream.size())

                val rotationCompensation = getCameraPreviewRotationCompensationFromCameraId(cameraId)
                bitmap = rotateBitmap(bitmap!!, rotationCompensation.toFloat())
            }

            return bitmap
        } catch (e: Exception) {
            logarErro(e, "Não foi possível converter frame da camera para bitmap:")
            return null
        }
    }

    /**
     * Cria um novo Bitmap rotacionado de acordo com um determinado ângulo (graus).
     */
    fun rotateBitmap(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
    }

    fun logSupportedPreviewFormats(camera: Camera) {
        camera.parameters.supportedPreviewFormats
                .map(CameraUtils::getSupportedPreviewFormatLabel)
                .also {
                    logarInfo("Supported Preview Formats: $it")
                }

    }

    fun logSupportedPreviewSizes(camera: Camera) {
        logarInfo("Supported Preview Sizes:")

        camera.parameters.supportedPreviewSizes
                .map { "(${it.width}, ${it.height})" }
                .forEach { logarInfo(it) }
    }

    fun logSupportedFpsRange(camera: Camera) {
        logarInfo("Supported Preview Fps Range:")

        val formattedRange = camera.parameters.supportedPreviewFpsRange.map { fpsRange ->
            val fpsRangeInSecs = fpsRange
                    .map { it / 1_000 }
            "(min: ${fpsRangeInSecs[0]}, max: ${fpsRangeInSecs[1]})"
        }

        logarInfo(formattedRange.joinToString(prefix = "[", separator = ", ", postfix = "]"))
    }

    fun logPreferredPreviewSizeForVideo(camera: Camera) {
        camera.parameters.preferredPreviewSizeForVideo
                .let { "Preferred Preview Size For Video: (${it.width}, ${it.height})" }
                .also {
                    logarInfo(it)
                }
    }
}



