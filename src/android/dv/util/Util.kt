package br.gov.serpro.dv.util


import android.util.Log



/* Loga uma mensagem informativa */
fun logarInfo(info: String) {
       Log.i("DV", info)

}

/* Loga uma mensagem completa de erro no Crashlytics e no console */
fun logarErro(throwable: Throwable?, detalhesErro: String?) {

    var mensagemErro = ""

    if (throwable != null) {
        mensagemErro = throwable.javaClass.toString() + ":" + throwable.message
    }

    mensagemErro += detalhesErro
    Log.e("DV", mensagemErro)

}
