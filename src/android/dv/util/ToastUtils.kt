package br.gov.serpro.dv.util

import android.content.Context
import android.util.Log
import android.widget.Toast
import br.gov.serpro.dv.util.logarErro
import br.gov.serpro.dv.util.logarInfo

/**
 * TODO Detecção de Vida
 * Utilitário para exibição de Toasts
 */
object ToastUtils {

    fun logAndToast(
            context: Context?,
            msg: String,
            duration: Int = Toast.LENGTH_LONG,
            level: Int = Log.INFO,
            exception: Exception? = null
    ) {
        when (level) {
            Log.INFO -> logarInfo(msg)
            Log.ERROR -> logarErro(exception, msg)
        }

        if (context != null) {
            Toast.makeText(context, msg, duration).show()
        }
    }
}
