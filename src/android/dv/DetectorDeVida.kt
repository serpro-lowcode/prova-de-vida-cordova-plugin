package br.gov.serpro.dv

import android.util.Log
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions

/**
 * TODO Detecção de Vida
 * Classe que encapsula as regras da Detecção de Vida e simplifica o uso do detector de faces do
 * Firebase ML Kit.
 *
 * @property delegate [DetectorDeVida.Delegate] responsável por tratar os eventos da detecção
 * @property limites Comtém os limites percentuais a serem considerados durante a detecção
 */
class DetectorDeVida(private val delegate: Delegate?, private val limites: Limites = Limites()) {

    /**
     * Interface Delegate para tratamento dos eventos de Detecção de Vida.
     */
    interface Delegate {

        /**
         * Callback chamada quando nenhuma face é detectada ou quando o Id da Face mudou durante
         * o processo.
         */
        fun onNenhumaFaceDetectada()

        /**
         * Callback chamada quando multiplas faces são detectadas.
         */
        fun onMultiplasFacesDetectadas()

        /**
         * Callback chamada quando uma única face é detectada com sucesso.
         *
         * @param face Face identificada
         */
        fun onFaceUnicaDetectada(face: FirebaseVisionFace)

        /**
         * Callback chamada quando um sorriso é detectado com sucesso.
         *
         * @param face Face identificada
         */
        fun onSorrisoDetectado(face: FirebaseVisionFace)

        /**
         * Callback chamada quando o piscar do olho esquerdo é detectado com sucesso.
         *
         * @param face Face identificada
         */
        fun onPiscarOlhoEsquerdo(face: FirebaseVisionFace)

        /**
         * Callback chamada quando o piscar do olho direito é detectado com sucesso.
         *
         * @param face Face identificada
         */
        fun onPiscarOlhoDireito(face: FirebaseVisionFace)

        /**
         * Callback chamada em casos de erro. Sua implementação padrão apenas loga o erro.
         *
         * @param e Exceção identificada durante o processo de detecção
         */
        fun onErroNaDeteccao(e: Exception) {
            Log.e("DV","Erro ao detectar imagem: $e")
        }

        /**
         * Callback chamada quando selfie é detectado com sucesso.
         *
         * @param face Face identificada
         */
        fun onSelfieDetectado(face: FirebaseVisionFace)

    }

    /**
     * Enumeração dos possíveis Alvos de Detecção suportados.
     */
    private enum class AlvoDeteccao {

        /**
         * Este alvo implica em: Olhos abertos e sem sorriso
         */
        FACE_UNICA,

        /**
         * Este alvo implica em: Olhos abertos e sorrindo
         */
        SORRISO,

        /**
         * Este alvo implica em: Olho esquerdo fechado, direito aberto e qualquer sorriso
         */
        OLHO_ESQUERDO_FECHADO,

        /**
         * Este alvo implica em: Olho direito fechado, esquerdo aberto e qualquer sorriso
         */
        OLHO_DIREITO_FECHADO,

        /**
         * Este alvo implica em: Olhos abertos e sem sorriso
         */
        SELFIE
    }

    /**
     * Limites de Detecção. Devem estar entre [0.0f e 1.0f].
     */
    data class Limites(val olhoEsquerdo: Float = LIMITE_OLHO_ESQUERDO_DEFAULT,
                       val olhoDireito: Float = LIMITE_OLHO_DIREITO_DEFAULT,
                       val sorriso: Float = LIMITE_SORRISO_DEFAULT)

    companion object {
        private const val TAG = "DetectorDeVida"

        // Limites Padrões de reconhecimento
        const val LIMITE_OLHO_ESQUERDO_DEFAULT = 0.30f
        const val LIMITE_OLHO_DIREITO_DEFAULT = 0.60f
        const val LIMITE_SORRISO_DEFAULT = 0.60f
    }

    /**
     * Face Detector do Firebase.
     */
    private val detector: FirebaseVisionFaceDetector

    /**
     * Id da Face alvo deverá ser reconhecida na próxima detecção.
     */
    private var faceId: Int? = null

    /**
     * Alvo da próxima detecção.
     */
    private var alvoDeteccao = AlvoDeteccao.FACE_UNICA

    private var busy = false

    val isBusy: Boolean
        get() = this.busy

    init {
        // https://firebase.google.com/docs/ml-kit/android/detect-faces#1-configure-the-face-detector
        val options = FirebaseVisionFaceDetectorOptions.Builder()

                // Dectection Mode
                // Favor speed or accuracy when detecting faces.
                .setPerformanceMode(FirebaseVisionFaceDetectorOptions.ACCURATE)

                // Detect landmarks
                // Whether or not to attempt to identify facial "landmarks"
                // (eyes, ears, nose, cheeks, mouth)
                .setLandmarkMode(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)

                // Classify faces
                // Whether or not to classify faces into categories such as "smiling", and "eyes open".
                .setClassificationMode(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)

                // Minimum face size
                // Sets the smallest desired face size, expressed as a proportion of the width of the
                // head to the image width.
                .setMinFaceSize(0.5f)

                // Enable face tracking
                // Whether or not to assign faces an ID, which can be used to track faces across images.
                .enableTracking()
                .build()


        this.detector = FirebaseVision.getInstance().getVisionFaceDetector(options)
    }

    /**
     * Detecta faces a partir de uma [FirebaseVisionImage].
     */
    fun detectarFace(image: FirebaseVisionImage) {
        detectarFace(image, AlvoDeteccao.FACE_UNICA)
    }

    /**
     * Inicia a detecção do sorriso para a respectiva imagem e faceId.
     * Em caso de sucesso, a callback [Delegate.onSorrisoDetectado] será chamada.
     *
     * Caso o faceId detectado seja diferente do especificado, a callback
     * [Delegate.onNenhumaFaceDetectada] será chamada ao invés.
     *
     * @param image Imagem a ser analisada
     * @param faceId Id da Face identificada na última detecção
     */
    fun detectarSorriso(image: FirebaseVisionImage, faceId: Int) {
        detectarFace(image, AlvoDeteccao.SORRISO, faceId)
    }

    /**
     * Inicia a detecção do pisque do olho esquerdo para a respectiva imagem e faceId.
     * Em caso de sucesso, a callback [Delegate.onPiscarOlhoEsquerdo] será chamada.
     *
     * Caso o faceId detectado seja diferente do especificado, a callback
     * [Delegate.onNenhumaFaceDetectada] será chamada ao invés.
     *
     * @param image Imagem a ser analisada
     * @param faceId Id da Face identificada na última detecção
     */
    fun detectarPisqueEsquerdo(image: FirebaseVisionImage, faceId: Int) {
        detectarFace(image, AlvoDeteccao.OLHO_ESQUERDO_FECHADO, faceId)
    }

    /**
     * Inicia a detecção do pisque do olho direito para a respectiva imagem e faceId.
     * Em caso de sucesso, a callback [Delegate.onPiscarOlhoDireito] será chamada.
     *
     * Caso o faceId detectado seja diferente do especificado, a callback
     * [Delegate.onNenhumaFaceDetectada] será chamada ao invés.
     *
     * @param image Imagem a ser analisada
     * @param faceId Id da Face identificada na última detecção
     */
    fun detectarPisqueDireito(image: FirebaseVisionImage, faceId: Int) {
        detectarFace(image, AlvoDeteccao.OLHO_DIREITO_FECHADO, faceId)
    }

    /**
     * Detecta faces a partir de uma [FirebaseVisionImage].
     */
    fun detectarSelfie(image: FirebaseVisionImage, faceId: Int) {
        detectarFace(image, AlvoDeteccao.SELFIE, faceId)
    }

    /**
     * Detecta faces a partir de uma [FirebaseVisionImage].
     *
     * @param image Imagem a ser analisada
     * @param alvo Alvo da Detecção
     * @param faceId Id da Face identificada na última detecçao
     */
    private fun detectarFace(image: FirebaseVisionImage,
                             alvo: AlvoDeteccao = AlvoDeteccao.FACE_UNICA,
                             faceId: Int? = null) {
        this.busy = true
        this.faceId = faceId
        this.alvoDeteccao = alvo

        val detectionTask = this.detector.detectInImage(image)

        detectionTask.addOnSuccessListener(this::onDetectionSuccess)
        detectionTask.addOnFailureListener(this::onDetectionError)
    }

    private fun onDetectionError(e: Exception) {
        this.busy = false
        this.delegate?.onErroNaDeteccao(e)
    }

    /**
     * Callback de sucesso chamado pelo detector de faces do Firebase ML Kit.
     *
     * @see [detectarFace]
     * @see [FirebaseVisionFaceDetector.detectInImage]
     *
     * @param faces Lista de faces identificadas pelo detector do Firebase ML Kit
     */
    private fun onDetectionSuccess(faces: List<FirebaseVisionFace>) {
        try {
            val facesCount = faces.size
            Log.e("DV","$facesCount Face(s) Identificada(s)")

            when {
                facesCount == 0 -> dispatchNenhumaFaceDetectada()
                facesCount > 1 -> dispatchMultiplasFacesDetectadas()
                else -> {
                    val face = faces[0]

                    val primeiraDeteccao = this.faceId == null
                    val faceMudouDeIdDuranteDeteccao = (!primeiraDeteccao) && (this.faceId != face.trackingId)

                    // Caso o Id da Face mude durante a detecção, dispara o evento FaceUnicaDetectada
                    // para informar ao delegate que o processo de detecção deve ser recomeçado por
                    // esta nova face.
                    if (faceMudouDeIdDuranteDeteccao || !sorrisoComputado(face) || !olhosComputados(face)){
//                        dispatchFaceUnicaDetectada(face)
                        dispatchNenhumaFaceDetectada()
                        return
                    }

                    Log.e("DV","(sorriso: ${face.smilingProbability}, olho_esq: ${face.leftEyeOpenProbability}, olho_dir: ${face.rightEyeOpenProbability})")

                    when (this.alvoDeteccao) {

                        AlvoDeteccao.FACE_UNICA -> {
                            if (olhosComputados(face) && olhosAbertos(face)) {
                                dispatchFaceUnicaDetectada(face)
                            }
                        }

                        AlvoDeteccao.OLHO_ESQUERDO_FECHADO -> {
                            if (olhosComputados(face) && olhoEsquerdoFechado(face) && olhoDireitoAberto(face)) {
                                dispatchPiscarOlhoEsquerdo(face)
                            }
                        }

                        AlvoDeteccao.OLHO_DIREITO_FECHADO -> {
                            if (olhosComputados(face) && olhoEsquerdoAberto(face) && olhoDireitoFechado(face)) {
                                dispatchPiscarOlhoDireito(face)
                            }
                        }

                        AlvoDeteccao.SORRISO -> {
                            if (sorrisoComputado(face) && sorrindo(face)) {
                                dispatchSorrisoDetectado(face)
                            }
                        }

                        AlvoDeteccao.SELFIE -> {
                            if (olhosComputados(face) && olhosAbertos(face) && !sorrindo(face)) {
                                dispatchSelfieDetectado(face)
                            }
                        }
                    }
                }
            }
        } finally {
            this.busy = false
        }
    }

    /**
     * Despacha o evento de Nenhuma Face Detectada (também chamado em caso de mudança de face
     * durante o processo de detecção) para o delegate.
     */
    private fun dispatchNenhumaFaceDetectada() {
        this.faceId = null
        this.delegate?.onNenhumaFaceDetectada()
    }

    /**
     * Despacha o evento de Múltiplas Faces Detectadas para o delegate.
     */
    private fun dispatchMultiplasFacesDetectadas() {
        this.faceId = null
        this.delegate?.onMultiplasFacesDetectadas()
    }

    /**
     * Despacha o evento de Face Única Detectada para o delegate.
     *
     * @param face Face identificada
     */
    private fun dispatchFaceUnicaDetectada(face: FirebaseVisionFace) {
        this.faceId = face.trackingId
        this.delegate?.onFaceUnicaDetectada(face)
    }

    /**
     * Despacha o evento de Sorriso Detectado para o delegate.
     *
     * @param face Face identificada
     */
    private fun dispatchSorrisoDetectado(face: FirebaseVisionFace) {
        this.faceId = face.trackingId
        this.delegate?.onSorrisoDetectado(face)
    }

    private fun dispatchSelfieDetectado(face: FirebaseVisionFace) {
        this.faceId = face.trackingId
        this.delegate?.onSelfieDetectado(face)
    }

    /**
     * Despacha o evento de Piscar Olho Esquerdo para o delegate.
     *
     * @param face Face identificada
     */
    private fun dispatchPiscarOlhoEsquerdo(face: FirebaseVisionFace) {
        this.faceId = face.trackingId
        this.delegate?.onPiscarOlhoEsquerdo(face)
    }

    /**
     * Despacha o evento de Piscar Olho Direito para o delegate.
     *
     * @param face Face identificada
     */
    private fun dispatchPiscarOlhoDireito(face: FirebaseVisionFace) {
        this.faceId = face.trackingId
        this.delegate?.onPiscarOlhoDireito(face)
    }

    private fun sorrisoComputado(face: FirebaseVisionFace): Boolean =
            face.smilingProbability != FirebaseVisionFace.UNCOMPUTED_PROBABILITY

    private fun olhoEsquerdoComputado(face: FirebaseVisionFace): Boolean =
            face.leftEyeOpenProbability != FirebaseVisionFace.UNCOMPUTED_PROBABILITY

    private fun olhoDireitoComputado(face: FirebaseVisionFace): Boolean =
            face.rightEyeOpenProbability != FirebaseVisionFace.UNCOMPUTED_PROBABILITY

    private fun olhosComputados(face: FirebaseVisionFace): Boolean =
            olhoEsquerdoComputado(face) && olhoDireitoComputado(face)

    /**
     * Verifica se a face especificada está sorrindo.
     *
     * @param face Face a ser analisada
     */
    private fun sorrindo(face: FirebaseVisionFace): Boolean =
            face.smilingProbability in limites.sorriso..1.0f

    /**
     * Verifica se a face especificada está com o olho esquerdo aberto.
     *
     * @param face Face a ser analisada
     */
    private fun olhoEsquerdoAberto(face: FirebaseVisionFace): Boolean =
            face.leftEyeOpenProbability in limites.olhoEsquerdo..1.0f

    /**
     * Verifica se a face especificada está com o olho esquerdo fechado.
     *
     * @param face Face a ser analisada
     */
    private fun olhoEsquerdoFechado(face: FirebaseVisionFace): Boolean =
            face.leftEyeOpenProbability in 0.0f..limites.olhoEsquerdo

    /**
     * Verifica se a face especificada está com o olho direito aberto.
     *
     * @param face Face a ser analisada
     */
    private fun olhoDireitoAberto(face: FirebaseVisionFace): Boolean =
            face.rightEyeOpenProbability in limites.olhoDireito..1.0f


    /**
     * Verifica se a face especificada está com o olho direito fechado.
     *
     * @param face Face a ser analisada
     */
    private fun olhoDireitoFechado(face: FirebaseVisionFace): Boolean =
            face.rightEyeOpenProbability in 0.0f..limites.olhoDireito

    /**
     * Verifica se a face especificada está com ambos os olhos abertos.
     *
     * @param face Face a ser analisada
     */
    private fun olhosAbertos(face: FirebaseVisionFace): Boolean =
            olhoEsquerdoAberto(face) && olhoDireitoAberto(face)
}
